
gsap.to('.scroll::after', {repeat: -1, translateY: '10px'});

gsap.to("#soundIcon", {
    opacity:1,
    x: 450,
    duration:1.5,
    pin:true,
    scrollTrigger: {
        trigger:'#section2',
        start: 'bottom 1100',
        
    }
})

gsap.to('#section1box', {
    opacity:1,
    x: -450,
    duration:1.5,
    pin:true,
    scrollTrigger: {
        trigger:'#section2',
        start: 'bottom 1100',
        
    }
})

gsap.to("#audioIcon", {
    opacity:1,
    x: -380,
    duration:1.5,
    scrollTrigger: {
        trigger:'#section3',
        start: 'bottom 1100',
        
    }
})

gsap.to('#section3 #redBox', {
    opacity:1,
    x: 460,
    duration:1.5,
    scrollTrigger: {
        trigger:'#section3',
        start: 'bottom 1100',
       
    }
})

gsap.to("#micIcon", {
    opacity:1,
    x: 870,
    duration:1.5,
    scrollTrigger: {
        trigger:'#section4',
        start: 'bottom 1100',
        
    }
})

gsap.to('#section4 .sectionbox', {
    opacity:1,
    x: -460,
    duration:1.5,
    scrollTrigger: {
        trigger:'#section4',
        start: 'bottom 1100',
        
    }
})

gsap.to("#volumeIcon", {
    opacity:1,
    x: -380,
    duration:1.5,
    scrollTrigger: {
        trigger:'#section5',
        start: 'bottom 1110',
       
    }
})

gsap.to('#section5 .sectionbox', {
    opacity:1,
    x: 460,
    duration:1.5,
    scrollTrigger: {
        trigger:'#section5',
        start: 'bottom 1110',
        
    }
})


gsap.to('#headphones', {
    opacity:1,
    duration:1,
    scrollTrigger: {
        trigger:'#section6',
        start:'bottom 1120'
    }
})

gsap.from('#headphones', {
    x:-400,
    duration:1,
    scrollTrigger: {
        trigger:'#section6',
        start:'bottom 1120'
    }
})

gsap.to('.purchaseDetails', {
    opacity:1,
    duration:1,
    scrollTrigger: {
        trigger:'#section6',
        start:'bottom 1120'
    }
})

gsap.from('.purchaseDetails', {
    
    x:350,
    duration:1,
    scrollTrigger: {
        trigger:'#section6',
        start:'bottom 1120'
    }
})